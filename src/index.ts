// import { ConnectedDb, Db } from './db';
// import { mediaModel } from './media';
// import { allAttributes, filterWithPolygon, filterWithTimeRange, randomAttributes, randomDocument } from './mocks';
// import sizeof from 'object-sizeof';
// import { AuthorizationAttributes } from './entity.attributes';
// import { searchMedias } from './media.queries';

// const generateMedias = (db: ConnectedDb) => async (amount: number): Promise<void> => {
//     console.log(`generating media`);
//     const medias = Array.from({ length: amount }).map(() => randomDocument());
//     console.log(`inserting to db`);

//     await db.insert(medias, "media");
//     console.log(`done inserting`);
// };


// const main = async () => {
//     console.time("maps");

//     // const db = new Db("mongodb+srv://zevops1:mapad@stresstest.605vo.mongodb.net/test", 'test');
//     const db = new Db("mongodb://localhost:27017", 'test');
//     const connectedDb = await db.start();
//     const channelId = "channel1";


//     const s = await mediaModel.findOne({});
//     console.log(sizeof(s));

//     await generateMedias(connectedDb)(3);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);
//     // await generateMedias(connectedDb)(30000);

//     await connectedDb.createIndex("media", {
//         "timeRanges.startTimestamp": 1,
//         "timeRanges.endTimestamp": 1
//     });
//     await profileSearchMedia(1, connectedDb, true);

//     await connectedDb.removeIndex("media", "timeRanges.startTimestamp_1_timeRanges.endTimestamp_1");
//     await profileSearchMedia(1, connectedDb, false);
//     // const indexName = await connectedDb.createIndex("media", { "mediaData.sourceId": 1, "authorizationAttributes.a": 1 });
//     // const durationsWithIndex = await profileAllowedRangesWithSize(connectedDb, channelId);
//     // console.log(`avg duration with index = ${mean(durationsWithIndex)}`);

//     // await connectedDb.removeIndex("media", indexName);
//     // const durationsNoIndex = await profileAllowedRangesWithSize(connectedDb, channelId);
//     // console.log(`avg duration without index = ${mean(durationsNoIndex)}`);


//     await connectedDb.stop();

//     // await connectedClient.bindQueue("q", "e", "topic", consumer(connectedDb));
//     // await insertMedias(connectedDb)(100000);

//     // await sendMessages(connectedClient, connectedDb);

//     // console.time("fors")
//     // for(let i=0;i<5000;i++){
//     //     for(let j=0;j<5000;j++){
//     //         await connectedClient.publish("e", "topic", { i,j });
//     //     }
//     // }
//     // console.timeEnd("fors")
// };
// main();
// async function profileAllowedRangesWithSize(db: ConnectedDb, channelId: string) {
//     console.log(`channel id is ${channelId}`);

//     const allowedRanges = await Promise.all(Array.from({ length: 100 }).map(async () => {
//         const start = new Date().getTime();
//         const userAttributes = randomAttributes();
//         const results = await db.calcAllowedMedias(userAttributes);
//         const end = new Date().getTime();
//         return { userAttributes, durationMs: end - start, amountOfResults: results.length };
//     }
//     ));
//     allowedRanges.forEach(result => {
//         console.log(`${result.userAttributes.a.length}, ${result.userAttributes.b.length}, ${result.userAttributes.c.length} - ${result.durationMs} - ${result.amountOfResults}`);
//     });
//     return allowedRanges.map(x => x.durationMs);
// }

// const profileSearchMedia = async (numOfRequests: number, db: ConnectedDb, isIndexed: boolean) => {
//     const requests = Array.from({ length: numOfRequests }).map(() => filterWithPolygon);
//     const results = await Promise.all(
//         requests.map(async req => {
//             const start = new Date().getTime();
//             const medias = await searchMedias(db)(req);
//             const end = new Date().getTime();
//             return {
//                 filter: req,
//                 durationMs: end - start,
//                 numOfResults: medias.length
//             };
//         })
//     );

//     const avgDuration = results.map(x => x.durationMs).reduce((a, b) => a + b) / results.length;
//     console.log(`
//             isIndexed? ${isIndexed}
//             average duration: ${avgDuration / 1000}, 
//             num of results: ${results[0].numOfResults}
//         `);
// };