
import { v4 as uuid } from 'uuid';
import { AuthorizationAttributes } from "./entity.attributes";
import { Media } from "./media";
import { TimeRange } from "./time";
// @ts-ignore
import { point, polygon, lineString } from 'geojson-random';
import { name } from 'faker';
import { Filter } from './media.queries';
import { GeoTypes } from './geometry';


const channelsIds = ["channel1", "channel2","channel3","channel6","channel5",
"channel6", "channel7","channel8","channel9","channel10",
"channel11", "channel12","channel13","channel14","channel15"];
export const randomDocument = (): Media => ({
    channelId: randomChoice(channelsIds),
    createdAt: new Date(),
    name: name.firstName(),
    photographedLocations: {
        type: GeoTypes.GeometryCollection,
        geometries: Array.from({
            length: randomAmount(50)
        })
            .map(() => polygon(1, 4, 4,[29, 29,36, 36]).features[0].geometry)
    },
    sensorLocations: {
        type: GeoTypes.GeometryCollection,
        geometries: Array.from({
            length: randomAmount(50)
        })
            .map(() => point(1,[29.4, 34,35.8, 35.6]).features[0].geometry)
    },
    timeRanges: Math.random() > 0.8 ? randomTimeRanges(1) : randomTimeRanges(randomAmount(4) + 1),
    type: randomString(),
    updatedAt: new Date(),
    watchCount: 0,
    authorizationAttributes: randomAttributes()
});


// export const mockFilter = (channelsIds: string[]): Filter => ({
//     authorizationAttributes: randomAttributes(),
//     channelIds: [randomChoice(channelsIds)],
//     timeRange: randomTimeRanges(1)[0],
// });

export const filterWithTimeRange: Filter = {
    timeRange: { startTimestamp: 430000, endTimestamp: 450000 }
};

export const filterWithPolygon: Filter = {
    authorizationAttributes: {
        a: [],
        b: [],
        c: [],
        d: false
    },
    // channelIds: ["channel1", "channel2"],
    // timeRange: { startTimestamp: 150893, endTimestamp: 1618739397899993 },
    polygon: {
        "type": GeoTypes.Polygon,
        "coordinates": [
            [
                [
                    34.9,
                    33.1
                ],
                [
                    35.7,
                    33.3
                ],
                [
                    35,
                    29.5
                ],
                [
                    34.1,
                    31.2
                ],
                [
                    34.9,
                    33.1
                ]
            ]
        ]
    }
};

const randomString = () =>
    uuid();
const randomNumber = () => Math.floor(Math.random() * 100000);
const randomAmount = (max: number) => Math.floor(Math.random() * max);

export const randomUser = (id: number) => ({
    permissions: {
        battlefronts
    },
    id
});

export const randomTimeline = (rangesAmount: number): TimeRange[] => {
    let timeline: TimeRange[] = [];
    for (let i = 0; i < rangesAmount; i++) {
        let startTimestamp: number;
        if (i > 0) {
            startTimestamp = timeline[i - 1].endTimestamp;
        } else {
            startTimestamp = 0;
        }
        timeline.push({
            startTimestamp,
            endTimestamp: startTimestamp + Math.random() * 1000
        });
    }
    return timeline;
};


export const randomAttributes = (): AuthorizationAttributes => ({
    a: randomBattlefronts(),
    b: randomTags(),
    c: randomShoses(),
    d: randomIsBlacklisted()
});
export const allAttributes = (): AuthorizationAttributes => ({
    a: BATTLEFRONTS,
    b: TAGS,
    c: SHOSES,
    d: true
});

const MAX_NUM_OF_ATTRIBUTES = 10;
const BATTLEFRONTS = ["a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10", "a11", "a12", "a13", "a14"];
const TAGS = ["b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "b10", "b11", "b12", "b13", "b14"];
const SHOSES = ["c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12", "c13", "c14"];

const randomAttributesAmount = () => Math.floor(Math.random() * MAX_NUM_OF_ATTRIBUTES);
export const randomBattlefronts = () =>
    Array.from({ length: randomAttributesAmount() }).map(() => randomChoice(BATTLEFRONTS));
export const randomTags = () =>
    Array.from({ length: randomAttributesAmount() }).map(() => randomChoice(TAGS));
export const randomShoses = () =>
    Array.from({ length: randomAttributesAmount() }).map(() => randomChoice(SHOSES));
export const randomIsBlacklisted = () =>
    randomChoice([true, false]);

const randomChoice = <T>(list: T[]) => list[Math.floor(Math.random() * list.length)];

const users = Array.from({ length: 5000 }).map((_, idx) => idx.toString());
const battlefronts = Array.from({ length: 15 }).map((_, idx) => idx.toString());

const randomTimeRanges = (amount: number): TimeRange[] => {
    const twoYears = 63158601086
    const year2019 = 1555746459541
    const start = randomAmount(twoYears) + year2019;
    const durations = Array.from({ length: 2 * amount }).map(() => randomAmount(50000));
    const ranges: TimeRange[] = [];
    let currStart = start;
    for (let i = 0; i < durations.length; i += 2) {
        const currEnd = currStart + durations[i];
        ranges.push({ startTimestamp: currStart, endTimestamp: currEnd });
        currStart = currEnd + durations[i + 1];
    }
    return ranges;
};