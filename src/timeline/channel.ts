import { Document, model, Schema } from "mongoose";

export interface Channel {
    _id: string;
    name: string;
    permissionsTags: string[];
    shoses: string[];
    isBlacklisted: boolean;
}
export const channelModel = model<Channel & Document>("channel", new Schema({
    _id: String,
    name: String,
    permissionsTags: [String],
    shoses: [String],
    isBlacklisted: Boolean
}));