import { atLeastOneOf } from "../requirement";

describe("at least one of", () => {
    describe("no required values", () => {
        const schema = atLeastOneOf([]);
        test("array with some value => passes validation", () => {
            const testedValues = ["some value"];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("empty array => passes validation", () => {
            const testedValues = [];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
    });
    describe("one value", () => {
        const requiredValue = "me";
        const schema = atLeastOneOf([requiredValue]);
        test("array with only the required value => passes validation", () => {
            const testedValues = [requiredValue];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array without the required value => fails validation", () => {
            const testedValues = [requiredValue + "OH NO"];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array with two values, including the required one => passes validation", () => {
            const testedValues = [requiredValue + "OH NO", requiredValue];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
    });
    describe("multiple values", () => {
        const requiredValue1 = "me";
        const requiredValue2 = "you";
        const schema = atLeastOneOf([requiredValue1, requiredValue2]);
        test("array with only one of the required values => passes validation", () => {
            const testedValues = [requiredValue1];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array with two values such that one of them is required => passes validation", () => {
            const testedValues = [requiredValue1 + "AHHH", requiredValue1];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array without any of the required values => fails validation", () => {
            const testedValues = [requiredValue1 + "OH NO", requiredValue2 + "yikes"];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array with both required values => passes validation", () => {
            const testedValues = [requiredValue1, requiredValue2];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array with both required values and additional value => passes validation", () => {
            const testedValues = [requiredValue1, requiredValue2, "hi"];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
    });
});

