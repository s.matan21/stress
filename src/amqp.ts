import { Channel, connect, Connection } from 'amqplib'

export class AmqpClient {
    constructor(private readonly rabbithost: string) { }
    async start(): Promise<ConnectedAmqpClient> {
        const connection = await connect(this.rabbithost)
        const channel = await connection.createChannel();
        return new ConnectedAmqpClient(channel)
    }
}

export class ConnectedAmqpClient {
    constructor(private readonly channel: Channel) { 
        channel.prefetch(50)
    }

    private async assertExchange(exchangeName: string) {
        const exchange = await this.channel.assertExchange(exchangeName, "topic")
        return exchange.exchange;
    }

    private async assertQueue(queueName: string) {
        const queue = await this.channel.assertQueue(queueName);
        return queue.queue;
    }

    async bindQueue(queueName: string, exchangeName: string, topic: string, consumer: (msg: {}) => Promise<unknown>) {
        await this.assertExchange(exchangeName)
        await this.assertQueue(queueName)
        await this.channel.bindQueue(queueName, exchangeName, topic)
        this.channel.consume(queueName, async (msg) => {
            if (msg) {
                const msgJson = JSON.parse(msg.content.toString())
    
                try {
                    await consumer(msgJson);
                    this.channel.ack(msg);
                } catch (e) {
                    this.channel.nack(msg);
                }
            }
        })
    }

    async publish(exchangeName: string, topic: string, msg: {}): Promise<void> {
        await this.assertExchange(exchangeName);
        const msgBuffer = Buffer.from(JSON.stringify(msg))
        this.channel.publish(exchangeName, topic, msgBuffer)
    }
}