import { Document, model, Schema } from "mongoose";
import { AuthorizationAttributes, authorizationAttributesSchemaObj } from "../entity.attributes";
import { TimeRange, timeRangeSchema } from "../time";

export interface TimelineInterval extends AuthorizationAttributes {
    entityId: string;
    timeRange: TimeRange;
}
const timelineIntervalMongooseSchema = new Schema({
    ...authorizationAttributesSchemaObj,
    entityId: String,
    timeRange: timeRangeSchema
});
export const timelineIntervalModel = model<TimelineInterval & Document>("entityTimelineInterval", timelineIntervalMongooseSchema);