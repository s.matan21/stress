import * as Joi from 'joi';
import { Document, Schema } from 'mongoose';

export type Attribute = string;
const attributeSchema = String;
export const attributeJoiSchema = {
    id: Joi.string().required(),
    name: Joi.string().required()
}
const BATTLEFRONTS_KEY = "battlefronts";
const SHOSES_KEY = "shoses";
const TAGS_KEY = "tags";
const IS_BLACKLISTED_KEY = "isBlacklisted";
export type AttributesType = typeof BATTLEFRONTS_KEY | typeof SHOSES_KEY | typeof TAGS_KEY | typeof IS_BLACKLISTED_KEY;
export const attributesTypeJoiSchema = Joi.string().allow(BATTLEFRONTS_KEY, SHOSES_KEY, TAGS_KEY, IS_BLACKLISTED_KEY)
const authorizationAttributesObject = {
    [BATTLEFRONTS_KEY]: {} as any as Attribute[],
    [SHOSES_KEY]: {} as any as Attribute[],
    [TAGS_KEY]: {} as any as Attribute[],
    [IS_BLACKLISTED_KEY]: {} as any as boolean
}
export type AuthorizationAttributes = typeof authorizationAttributesObject;

export const authorizationAttributesSchemaObj = {
    [BATTLEFRONTS_KEY]: [attributeSchema],
    [SHOSES_KEY]: [attributeSchema],
    [TAGS_KEY]: [attributeSchema],
    [IS_BLACKLISTED_KEY]: Boolean
};
export const authorizationAttributesSchema = new Schema<AuthorizationAttributes & Document>(authorizationAttributesSchemaObj)