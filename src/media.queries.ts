import { FilterQuery } from "mongoose";
import { ConnectedDb } from "./db";
import { AuthorizationAttributes } from "./entity.attributes";
import { Polygon } from "./geometry";
import { Media, mediaModel } from "./media";
import { TimeRange } from "./time";

export interface Filter {
    polygon?: Polygon;
    timeRange?: TimeRange;
    channelIds?: string[];
    authorizationAttributes?: AuthorizationAttributes;
}

export const searchMedias = (db: ConnectedDb) => (filter: Filter) => {
    const x = [
        {
            $match: {
                ...(filter.polygon ? polygonIntersecting(filter.polygon) : {}),
                ...(filter.timeRange ? timeRangeIntersecting(filter.timeRange) : {}),
                ...(filter.channelIds ? { channelId: { $in: filter.channelIds } } : {}),
            }
        },
        ...(filter.authorizationAttributes ? [db.generateAuthorizationCheck(filter.authorizationAttributes)] : []),
    ];
    return mediaModel.aggregate(x);
};

const timeRangeIntersecting = (timeRange: TimeRange) => ({
    timeRanges: {
        $elemMatch: {
            $or: [
                {
                    startTimestamp: { $lte: timeRange.startTimestamp },
                    endTimestamp: { $gte: timeRange.startTimestamp }
                },
                {
                    startTimestamp: { $lte: timeRange.endTimestamp },
                    endTimestamp: { $gte: timeRange.endTimestamp }
                },
                {
                    startTimestamp: { $lte: timeRange.startTimestamp },
                    endTimestamp: { $gte: timeRange.endTimestamp }
                },
                {
                    startTimestamp: { $gte: timeRange.startTimestamp },
                    endTimestamp: { $lte: timeRange.endTimestamp }
                },
            ]
        }
    }

});

const polygonIntersecting = (polygon: Polygon) => ({
    $or: [
        {
            sensorLocations: {
                $geoIntersects: {
                    $geometry: polygon
                }
            },
        },
        {
            photographedLocations: {
                $geoIntersects: {
                    $geometry: polygon
                }
            }
        }
    ]
});