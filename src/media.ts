import { Document, model, Schema, Types } from "mongoose";
import { AuthorizationAttributes, authorizationAttributesSchemaObj } from "./entity.attributes";
import { Geometry, GeometryCollection } from "./geometry";
import { TimeRange } from "./time";

export const basicMediaSchema = new Schema({


});




export interface Media {
    name: string,
    channelId: string,
    watchCount: number,
    type: string,
    createdAt: Date,
    updatedAt: Date,
    timeRanges: TimeRange[];
    sensorLocations: GeometryCollection;
    photographedLocations: GeometryCollection,
    authorizationAttributes: AuthorizationAttributes;

}
export const mediaModel = model<Media & Document>("media", new Schema({
    name: String,
    channelId: String,
    watchCount: Number,
    type: String,
    createdAt: Date,
    updatedAt: Date,
    timeRanges: [{
        startTimestamp: Number,
        endTimestamp: Number,
    }],
    sensorLocations: [{
        type: String,
        geometry: Schema.Types.Mixed
    }],
    photographedLocations: [{
        type: String,
        geometry: Schema.Types.Mixed
    }],
    authorizationAttributes: authorizationAttributesSchemaObj,

}));