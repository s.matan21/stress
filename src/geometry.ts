export type LongLatPoint = [number, number];
export type LineStringPoints = LongLatPoint[];
export type PolygonPoints = LineStringPoints[];

export enum GeoTypes {
    Point = "Point",
    Polygon = "Polygon",
    LineString = "LineString",
    GeometryCollection = "GeometryCollection"
}

export interface GeoJson {
    type: "Feature",
    properties?: { [key: string]: any },
    geometry: Geometry | GeometryCollection,
}

export interface Geometry {
    type: GeoTypes,
    coordinates: LongLatPoint | LineStringPoints | PolygonPoints
}

export interface Point extends Geometry {
    type: GeoTypes,
    coordinates: LongLatPoint
}

export interface LineString extends Geometry {
    type: GeoTypes,
    coordinates: LineStringPoints
}

export interface Polygon extends Geometry {
    type: GeoTypes,
    coordinates: PolygonPoints
}

export interface GeometryCollection {
    type: "GeometryCollection",
    geometries: Geometry[]
}