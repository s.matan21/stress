import { connect, Connection } from 'mongoose';
import { ObjectId } from 'mongodb';
import { mediaModel } from './media';
import { AuthorizationAttributes } from './entity.attributes';

export class Db {
    constructor(private readonly hostname: string, private readonly dbName: string) { }

    async start() {
        const { connection } = await connect(this.hostname, { dbName: this.dbName });
        return new ConnectedDb(connection);
    }
}

export class ConnectedDb {
    constructor(private readonly connection: Connection) { }

    insert(docs: {}[], collectionName: string) {
        return this.connection.collection(collectionName).insertMany(docs);
    }

    find(collectionName: string, fromMediaId: string, pageSize: number) {
        return this.connection.collection(collectionName).find({ _id: { $gt: new ObjectId(fromMediaId) } }, { limit: pageSize }).project({ _id: 1 }).toArray();
    }

    drop(collectionName: string) {
        return this.connection.collection(collectionName).drop();
    }

    createIndex(collectionName: string, fields: {}) {
        return this.connection.collection(collectionName).createIndex(fields);
    }
    removeIndex(collectionName: string, indexName: string) {
        return this.connection.collection(collectionName).dropIndex(indexName);
    }

    findAllMedias = () => mediaModel.find({});

    calcAllowedMedias = (userAttributes: AuthorizationAttributes) =>
        mediaModel.aggregate([
            this.generateAuthorizationCheck(userAttributes)
        ]);

    public generateAuthorizationCheck(userAttributes: { a: string[]; b: string[]; c: string[]; d: boolean; }): any {
        return {
            $match: {
                $or: [
                    { "authorizationAttributes.a": [] },
                    { "authorizationAttributes.a": { $in: userAttributes.a } },
                ],
                $expr: {
                    $and: [
                        { $setIsSubset: ["$authorizationAttributes.b", userAttributes.b] },
                        { $setIsSubset: ["$authorizationAttributes.c", userAttributes.c] }
                    ]
                }
            }
        };
    }

    stop() {
        return this.connection.close();
    }
}