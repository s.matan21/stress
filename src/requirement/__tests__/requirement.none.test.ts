import { noneOf } from "../requirement";

describe("all of", () => {
    describe("no required values", () => {
        const schema = noneOf([]);
        test("array with some value => passes validation", () => {
            const testedValues = ["some value"];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("empty array => passes validation", () => {
            const testedValues = [];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
    });
    describe("one value", () => {
        const requiredValue = "me";
        const schema = noneOf([requiredValue]);
        test("array with only the required value => fails validation", () => {
            const testedValues = [requiredValue];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array without the required value => passes validation", () => {
            const testedValues = [requiredValue + "OH NO"];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array with two values, including the required one => fails validation", () => {
            const testedValues = [requiredValue + "OH NO", requiredValue];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
    });
    describe("multiple values", () => {
        const requiredValue1 = "me";
        const requiredValue2 = "you";
        const schema = noneOf([requiredValue1, requiredValue2]);
        test("array with only one of the required values => fails validation", () => {
            const testedValues = [requiredValue1];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array with two values such that only one of them is required => fails validation", () => {
            const testedValues = [requiredValue1 + "hi", requiredValue1];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array without any of the required values => passes validation", () => {
            const testedValues = [requiredValue1 + "OH NO", requiredValue2 + "yikes"];
            const { error } = schema.validate(testedValues);
            expect(error).toBeUndefined();
        });
        test("array with both required values => fails validation", () => {
            const testedValues = [requiredValue1, requiredValue2];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
        test("array with both required values and additional value => fails validation", () => {
            const testedValues = [requiredValue1, requiredValue2, "hi"];
            const { error } = schema.validate(testedValues);
            expect(error).not.toBeUndefined();
        });
    });
});