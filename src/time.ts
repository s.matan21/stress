export interface TimeRange {
    startTimestamp: number;
    endTimestamp: number;
}
export const timeRangeSchema = {
    startTimestamp: Number,
    endTimestamp: Number
}