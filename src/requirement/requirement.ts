import Joi from 'joi';

export const atLeastOneOf = (values: string[]) =>
    values.length == 0 ?
        Joi.array() :
        Joi.array().items(Joi.string().valid(...values).required(), Joi.string());

export const allOf = (values: string[]) =>
    values.length == 0 ?
        Joi.array() :
        Joi.array().items(...values.map(value => Joi.string().valid(value).required()), Joi.string());

export const noneOf = (values: string[]) =>
    values.length == 0 ?
        Joi.array() :
        Joi.array().items(Joi.invalid(...values));